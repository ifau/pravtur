//
//  Constants.swift
//  pravtur
//
//  Created by ifau on 15/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class Constants: NSObject
{
    class func blueColor() -> UIColor
    {
        return UIColor(red: 0.0/255.0, green:160/255.0, blue:200/255.0, alpha: 1.0)
    }
}
