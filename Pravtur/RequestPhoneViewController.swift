//
//  RequestPhoneViewController.swift
//  pravtur
//
//  Created by ifau on 11/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RequestPhoneViewController: UIViewController
{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var loadingActivityView: UIActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadingActivityView.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        selectTourSharedDataSource.sendPhoneRequestToRemoteServer
        { [weak self] (success: Bool) -> () in
                
            if let strongSelf = self
            {
                if success
                {
                    strongSelf.titleLabel.text = "Заявка успешно отправлена"
                    strongSelf.loadingActivityView.stopAnimating()
                    strongSelf.loadingActivityView.isHidden = true
                    strongSelf.contentSizeInPopup = CGSize(width: 280, height: 50)
                }
                else
                {
                    strongSelf.titleLabel.text = "Не удалось отправить заявку. Возможно отсутствует соединение с интернетом или сервер недоступен. Попробуйте еще раз."
                    strongSelf.loadingActivityView.stopAnimating()
                    strongSelf.loadingActivityView.isHidden = true
                }
            }
        }
    }
}
