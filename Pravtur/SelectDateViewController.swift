//
//  SelectDateViewController.swift
//  pravtur
//
//  Created by ifau on 23/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import FSCalendar

class SelectDateViewController: UIViewController, YSRangeSLiderDelegate, FSCalendarDelegate
{
    @IBOutlet fileprivate var calendarView: FSCalendar!
    @IBOutlet fileprivate var sliderView: YSRangeSlider!
    @IBOutlet fileprivate var nightsLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sliderView.minimumValue = 1
        sliderView.maximumValue = 29
        sliderView.minimumSelectedValue = CGFloat(selectTourSharedDataSource.startNight)
        sliderView.maximumSelectedValue = CGFloat(selectTourSharedDataSource.endNight)
        sliderView.leftThumbColor = Constants.blueColor()
        sliderView.rightThumbColor = Constants.blueColor()
        sliderView.sliderLineColorBetweenThumbs = Constants.blueColor()
        sliderView.sliderLineColor = UIColor.lightGray
        sliderView.delegate = self
        
        calendarView.appearance.headerTitleColor = Constants.blueColor()
        calendarView.appearance.weekdayTextColor = Constants.blueColor()
        calendarView.appearance.selectionColor = Constants.blueColor()
        calendarView.appearance.todayColor = UIColor.lightGray
        calendarView.select(selectTourSharedDataSource.date)
        calendarView.delegate = self
        
        nightsLabel.textColor = Constants.blueColor()
        updateLabel()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date)
    {
        selectTourSharedDataSource.date = date
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date) -> Bool
    {
        if date.compare(Date()) == ComparisonResult.orderedDescending // date after current date
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func rangeSliderDidChange(_ rangeSlider: YSRangeSlider, minimumSelectedValue: CGFloat, maximumSelectedValue: CGFloat)
    {
        selectTourSharedDataSource.startNight = Int(minimumSelectedValue)
        selectTourSharedDataSource.endNight = Int(maximumSelectedValue)
        updateLabel()
    }
    
    func updateLabel()
    {
        nightsLabel.text = "От \(selectTourSharedDataSource.startNight) до \(selectTourSharedDataSource.endNight) ночей"
    }
}
