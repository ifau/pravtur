//
//  RequestTourViewController.swift
//  pravtur
//
//  Created by ifau on 07/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RequestTourViewController: UIViewController
{
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var bonusTextField: UITextField!
    
    @IBOutlet var sendButton: UIButton!
    
    @IBOutlet var loadingIndicatorView: UIActivityIndicatorView!
    
    var tour: Tour?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        resignLoadingState()
        
        sendButton.backgroundColor = Constants.blueColor()
        sendButton.layer.cornerRadius = 4
        
        nameTextField.tag = 0
        nameTextField.text = selectTourSharedDataSource.name
        nameTextField.addTarget(self, action: #selector(RequestTourViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        phoneTextField.tag = 1
        phoneTextField.text = selectTourSharedDataSource.phone
        phoneTextField.addTarget(self, action: #selector(RequestTourViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        bonusTextField.tag = 2
        bonusTextField.text = selectTourSharedDataSource.bonus
        bonusTextField.addTarget(self, action: #selector(RequestTourViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        nameTextField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        view.endEditing(true)
    }
    
    func textFieldDidChange(_ textField: UITextField)
    {
        if textField.tag == 0
        {
            selectTourSharedDataSource.name = textField.text
        }
        else if textField.tag == 1
        {
            selectTourSharedDataSource.phone = textField.text
        }
        else if textField.tag == 2
        {
            selectTourSharedDataSource.bonus = textField.text
        }
    }
    
    func becomeLoadingState()
    {
        nameTextField.isEnabled = false
        phoneTextField.isEnabled = false
        bonusTextField.isEnabled = false
        
        sendButton.setTitle("", for: UIControlState())
        loadingIndicatorView.isHidden = false
        loadingIndicatorView.startAnimating()
    }
    
    func resignLoadingState()
    {
        titleLabel.text = tour == nil ? "Заполните форму ниже, чтобы наши менеджеры могли с Вами связаться и подобрать самый оптимальный тур" : "\nЗаполните форму ниже, для заказа выбранного Вами тура"
        self.contentSizeInPopup = CGSize(width: 280, height: 300)
        
        nameTextField.isEnabled = true
        phoneTextField.isEnabled = true
        bonusTextField.isEnabled = true
        
        loadingIndicatorView.stopAnimating()
        loadingIndicatorView.isHidden = true
        sendButton.setTitle("Отправить", for: UIControlState())
    }
    
    func becomeSuccessState()
    {
        titleLabel.text = "\n\n\nВаша заявка успешно отправлена\n\n\n"
        self.contentSizeInPopup = CGSize(width: 280, height: 120)
    }
    
    func becomeFailedState()
    {
        titleLabel.text = "\n\nНе удалось отправить заявку. Возможно отсутствует соединение с интернетом или сервер недоступен. Попробуйте еще раз.\n\n"
        self.contentSizeInPopup = CGSize(width: 280, height: 120)
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        if selectTourSharedDataSource.phone.characters.count > 5
        {
            becomeLoadingState()
            
            let completion = { [weak self] (success: Bool) -> () in
                
                if let strongSelf = self
                {
                    if success
                    {
                        strongSelf.becomeSuccessState()
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
                        execute: {
                            strongSelf.popupController?.dismiss()
                        })
                    }
                    else
                    {
                        strongSelf.becomeFailedState()
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
                        execute: {
                            strongSelf.resignLoadingState()
                        })
                    }
                }
            }
            
            if tour != nil
            {
                selectTourSharedDataSource.sendHotTourRequestToRemoteServer(tour!, completion: completion)
            }
            else
            {
                selectTourSharedDataSource.sendTourRequestToRemoteServer(completion)
            }
        }
        else
        {
            UIView.animate(withDuration: 0.25, animations:
            { () -> Void in
                
                self.phoneTextField.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.4)
            },
            completion:
            { (finished: Bool) -> Void in
                
                UIView.animate(withDuration: 0.25, animations:
                { () -> Void in
                    
                    self.phoneTextField.backgroundColor = UIColor.white
                })
            })
        }
    }
}
