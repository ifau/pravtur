//
//  MTOURSObject.swift
//  pravtur
//
//  Created by ifau on 23/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class MTOURSObject: NSObject
{
    var count : String!
    var tours : [Tour]!

    init(fromDictionary dictionary: NSDictionary)
    {
        count = dictionary["count"] as? String
        tours = [Tour]()
        if let toursArray = dictionary["tours"] as? [NSDictionary]
        {
            for dic in toursArray
            {
                let value = Tour(fromDictionary: dic)
                tours.append(value)
            }
        }
    }
}

class Tour
{
    var additionally : String!
    var contents : String!
    var country : String!
    var created : String!
    var departure : String!
    var hotel : String!
    var id : String!
    var operator_ : String!
    var period : String!
    var preview : String!
    var price : String!
    var resort : String!
    var tourType : String!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        additionally = dictionary["additionally"] as? String
        contents = dictionary["contents"] as? String
        country = dictionary["country"] as? String
        created = dictionary["created"] as? String
        departure = dictionary["departure"] as? String
        hotel = dictionary["hotel"] as? String
        id = dictionary["id"] as? String
        operator_ = dictionary["operator"] as? String
        period = dictionary["period"] as? String
        preview = dictionary["preview"] as? String
        price = dictionary["price"] as? String
        resort = dictionary["resort"] as? String
        tourType = dictionary["tourType"] as? String
    }
}
