//
//  TextFieldTableViewCell.swift
//  pravtur
//
//  Created by ifau on 18/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell
{

    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        firstLabel.textColor = UIColor.white
        textField.textColor = UIColor.white
        textField.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.1)
        textField.layer.cornerRadius = 15
    }
}
