//
//  RootViewController.swift
//  pravtur
//
//  Created by ifau on 15/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import STPopup

class RootViewController: UIViewController
{
    fileprivate var menuView: BTNavigationDropdownMenu!
    @IBOutlet var contentView: UIView!
    fileprivate var viewController: UIViewController?
    fileprivate let identifiers = ["HotToursVC", "SelectTourVC", "NewsVC", "ContactsVC", "AboutVC"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let items = ["Горящие туры", "Запрос на подбор тура", "Новости", "Контакты", "О компании"]
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Constants.blueColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, title: items.first!, items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler =
        { [unowned self] (indexPath: Int) -> () in
            
            self.displayContentWithIdentifier(self.identifiers[indexPath])
        }
        
        self.navigationItem.titleView = menuView
        
        STPopupNavigationBar.appearance().barTintColor = Constants.blueColor()
        STPopupNavigationBar.appearance().tintColor = UIColor.white
        STPopupNavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: "Avenir-Heavy", size: 16)!, NSForegroundColorAttributeName: UIColor.white]
        //UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([STPopupNavigationBar.self]).setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Avenir-Heavy", size: 14)!, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Normal)
        displayContentWithIdentifier(identifiers.first!)
    }
    
    func displayContentWithIdentifier(_ identifier: String)
    {
        viewController?.willMove(toParentViewController: nil)
        viewController?.view.removeFromSuperview()
        viewController?.removeFromParentViewController()
        viewController?.didMove(toParentViewController: nil)
        
        viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
        
        viewController?.willMove(toParentViewController: self)
        self.addChildViewController(viewController!)
        viewController!.view.frame = contentView.bounds
        contentView.addSubview(viewController!.view)
        viewController!.didMove(toParentViewController: self)
    }
}
