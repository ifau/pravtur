//
//  TextLabelTableViewCell.swift
//  pravtur
//
//  Created by ifau on 16/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class TextLabelTableViewCell: UITableViewCell
{

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var secondLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        firstLabel.textColor = UIColor.white
        secondLabel.textColor = UIColor.white
    }
}
