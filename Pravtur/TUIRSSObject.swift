//
//  TUIRSSObject.swift
//  pravtur
//
//  Created by ifau on 02/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import SWXMLHash

class TUIRSSObject: NSObject
{
    var items: [RSSItem] = []
    
    init(fromData data: Data)
    {
        let xml = SWXMLHash.parse(data)
        
        for element in xml["rss"]["channel"]["item"]
        {
            let title = element["title"].element?.text!
            let description = element["description"].element?.text!
            let link = element["link"].element?.text!
            let pubDate = element["pubDate"].element?.text!
            
            let item = RSSItem(title: title!, description: description!, link: link!, pubDate: pubDate!)
            items.append(item)
        }
    }
}

class RSSItem: NSObject
{
    let html: String!
    let url: URL!
    var contentHeigh: CGFloat = 0.0
    
    init(title: String, description: String, link: String, pubDate: String)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: pubDate)
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let content = "\(dateFormatter.string(from: date!))<h3>\(title)</h3><p>\(description)</p>"
        html = "<html><head><style type='text/css'>body{font-family:'Helvetica Neue';font-size:14px;color:white;}</style></head><body>\(content)</body></html>"
        
        let encodedURL = link.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let x = encodedURL.replacingOccurrences(of: "http%3A%2F%2Fwww.tui.ru%2FNews%2F", with: "http://www.tui.ru/News/")
        url = URL(string: x)
    }
}
