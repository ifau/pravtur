//
//  NewsViewController.swift
//  pravtur
//
//  Created by ifau on 02/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate
{
    @IBOutlet var tableView: UITableView!
    var rss: TUIRSSObject?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "WebViewTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
        requestRSS()
    }
    
    func requestRSS()
    {
        let session = URLSession.shared
        let str = "http://www.tui.ru/CMSPages/NewsRss.aspx"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        tableView.alpha = 0.0
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let task = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        strongSelf.rss = TUIRSSObject(fromData: data!)
                        strongSelf.tableView.reloadData()
                        UIView.animate(withDuration: 0.5, delay: 2.0, options: UIViewAnimationOptions.curveLinear, animations:
                        {
                            strongSelf.tableView.alpha = 1.0
                        },
                        completion:
                        { (Bool) -> Void in
                            
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        })
                    })
                }
                else
                {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    })
                }
            }
        })

        task.resume()
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return rss == nil ? 0 : rss!.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let item = rss!.items[indexPath.row]
        return item.contentHeigh == 0 ? 44.0 : item.contentHeigh
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let _cell = cell as! WebViewTableViewCell
        let item = rss!.items[indexPath.row]
        
        _cell.webView.tag = indexPath.row
        _cell.webView.delegate = self
        _cell.webView.isUserInteractionEnabled = false
        _cell.webView.loadHTMLString(item.html, baseURL: nil)
        _cell.webView.isOpaque = false
        _cell.webView.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let item = rss!.items[indexPath.row]
        UIApplication.shared.openURL(item.url)
    }
    
    // MARK: - UIWebViewDelegate
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        let item = rss!.items[webView.tag]
        
        if (item.contentHeigh != 0.0)
        {
            return
        }
        
        item.contentHeigh = webView.scrollView.contentSize.height
        tableView.reloadRows(at: [IndexPath(row: webView.tag, section: 0)], with: .automatic)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        if navigationType == UIWebViewNavigationType.linkClicked
        {
            UIApplication.shared.openURL(request.url!)
            return false
        }
        return true
    }
}
