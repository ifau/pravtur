//
//  SegmentSwitchTableViewCell.swift
//  pravtur
//
//  Created by ifau on 16/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class SegmentSwitchTableViewCell: UITableViewCell
{
    @IBOutlet weak var segmentSwitch: AnimatedSegmentSwitch!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        segmentSwitch.backgroundColor = Constants.blueColor()
        segmentSwitch.selectedTitleColor = Constants.blueColor()
        segmentSwitch.cornerRadius = 17
        segmentSwitch.thumbCornerRadius = 17
        segmentSwitch.font = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
    }
}
