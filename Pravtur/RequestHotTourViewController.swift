//
//  RequestHotTourViewController.swift
//  pravtur
//
//  Created by ifau on 05/05/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RequestHotTourViewController: UIViewController
{
    var tour: Tour!
    
    @IBOutlet var infoTextView: UITextView!
    @IBOutlet var sendButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sendButton.setTitleColor(Constants.blueColor(), for: UIControlState())
        sendButton.backgroundColor = UIColor.white
        sendButton.layer.borderColor = Constants.blueColor().cgColor
        sendButton.layer.borderWidth = 1
        sendButton.layer.cornerRadius = 4
        
        infoTextView.attributedText = generateDescription()
    }
    
    func generateDescription() -> NSAttributedString
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: tour.departure)!
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        let line1 = "\(tour.country!) за \(tour.price!) ₽"
        let line2 = "Курорт: \(tour.resort!)"
        let line3 = "Дней: \(tour.period!)"
        let line4 = "Отель: \(tour.hotel!)"
        let line5 = "В стоимость тура входит: \(tour.contents!)"
        let line6 = "Дата вылета: \(dateFormatter.string(from: date))"
        let line7 = "Цена указана на 1 человека при 2-местном размещении."
        
        let descriptionText = "\(line1)\n\n\(line2)\n\n\(line3)\n\n\(line4)\n\n\(line5)\n\n\(line6)\n\n\(line7)"
        
        let attributedDescription = NSMutableAttributedString(string: descriptionText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 12)!, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: descriptionText.characters.count))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        attributedDescription.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: (descriptionText as NSString).range(of: line1))
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Medium", size: 14)!, range: (descriptionText as NSString).range(of: line1))
        
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.blueColor(), range: (descriptionText as NSString).range(of: line1))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.blueColor(), range: (descriptionText as NSString).range(of: "Курорт:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.blueColor(), range: (descriptionText as NSString).range(of: "Дней:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.blueColor(), range: (descriptionText as NSString).range(of: "Отель:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.blueColor(), range: (descriptionText as NSString).range(of: "В стоимость тура входит:"))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: Constants.blueColor(), range: (descriptionText as NSString).range(of: "Дата вылета:"))
        
        return attributedDescription
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RequestVC") as! RequestTourViewController
        viewController.tour = tour
        self.popupController?.push(viewController, animated: true)
    }
}
