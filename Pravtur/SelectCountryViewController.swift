//
//  SelectCountryViewController.swift
//  pravtur
//
//  Created by ifau on 24/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class SelectCountryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    fileprivate let countries = ["Любая страна", "Болгария", "Греция", "Испания", "Италия", "Кипр", "Россия", "Хорватия", "Черногория"]
    fileprivate var previousIndexPath: IndexPath!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
        
        let selectedIndex = countries.index(of: selectTourSharedDataSource.country)!
        previousIndexPath = IndexPath(row: selectedIndex, section: 0)
    }
    
    func isSelectedRowAtIndex(_ index: Int) -> Bool
    {
        return (selectTourSharedDataSource.country == countries[index]) ? true : false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.tintColor = Constants.blueColor()
        cell.textLabel?.text = countries[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if isSelectedRowAtIndex(indexPath.row)
        {
            cell.textLabel?.textColor = Constants.blueColor()
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        else
        {
            cell.textLabel?.textColor = UIColor.gray
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if previousIndexPath != indexPath
        {
            selectTourSharedDataSource.country = countries[indexPath.row]
            tableView.reloadRows(at: [previousIndexPath, indexPath], with: .automatic)
            previousIndexPath = indexPath
        }
    }
}
