//
//  ButtonTableViewCell.swift
//  pravtur
//
//  Created by ifau on 18/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell
{
    @IBOutlet var button: UIButton!
    
    var title: String = ""
    {
        didSet
        {
            button.setTitle(title, for: UIControlState())
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        button.layer.borderWidth = 2.0
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 22
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
    }
}
