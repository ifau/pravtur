//
//  HotTourTableViewCell.swift
//  pravtur
//
//  Created by ifau on 23/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class HotTourTableViewCell: UITableViewCell
{
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var resortLabel: UILabel!
    @IBOutlet var periodLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var previewImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
