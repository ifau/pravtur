//
//  HotToursViewController.swift
//  pravtur
//
//  Created by ifau on 23/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import Kingfisher
import STPopup

class HotToursViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var mtours: MTOURSObject?
    var filteredTours: [Tour]?
    fileprivate var selectedSegmentIndex = 0
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HotTourTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.register(UINib(nibName: "SegmentSwitchTableViewCell", bundle: nil), forCellReuseIdentifier: "Header")
        tableView.dataSource = self
        tableView.delegate = self
        requestTours()
    }
    
    var loadingView: UIView?
    
    func showLoadingView()
    {
        if (loadingView != nil)
        {
            loadingView!.removeFromSuperview()
        }
        
        loadingView = UIView()
        loadingView?.backgroundColor = UIColor.white
        loadingView?.layer.cornerRadius = 12
        loadingView?.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        
        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        loadingIndicator.color = Constants.blueColor()
        loadingIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        loadingIndicator.center = loadingView!.center
        loadingView?.addSubview(loadingIndicator)
        
        let loadingLabel = UILabel()
        loadingLabel.textColor = Constants.blueColor()
        loadingLabel.textAlignment = .center
        loadingLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        loadingLabel.text = "Подождите.."
        loadingLabel.frame = CGRect(x: 0, y: loadingIndicator.frame.origin.y + 48, width: loadingView!.frame.size.width, height: 20)
        loadingView?.addSubview(loadingLabel)
        
        loadingView?.center = self.view.center
        loadingView?.frame = CGRect(x: loadingView!.frame.origin.x, y: loadingView!.frame.origin.y - 48, width: loadingView!.frame.size.width, height: loadingView!.frame.size.height)
        
        self.view.addSubview(loadingView!)
        loadingIndicator.startAnimating()
    }
    
    func hideLoadingView()
    {
        if (loadingView != nil)
        {
            loadingView!.removeFromSuperview()
            loadingView = nil
        }
    }
    
    func showMessage(title: String?, message: String?)
    {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(ac, animated: true, completion: nil)
    }
    
    func requestTours()
    {
        let session = URLSession.shared
        let str = "https://mtours.ru/api/hot-tours.json?key=6980369d9bfde5ab89c89c498401ffd2"
        let url = URL(string: str)!
        let request = URLRequest(url: url)
        
        tableView.alpha = 0.0
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        showLoadingView()
        
        let task = session.dataTask(with: request, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            if let strongSelf = self
            {
                if error == nil && data != nil
                {
                    DispatchQueue.main.async(execute: {
                        let jsonDictionary = try! JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                        strongSelf.mtours = MTOURSObject(fromDictionary: jsonDictionary)
                        strongSelf.filteredTours = strongSelf.mtours?.tours
                        strongSelf.tableView.reloadData()
                        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations:
                        {
                            strongSelf.tableView.alpha = 1.0
                        },
                        completion:
                        { (Bool) -> Void in
                                        
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            strongSelf.hideLoadingView()
                        })
                    })
                }
                else
                {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        strongSelf.hideLoadingView()
                        strongSelf.showMessage(title: nil, message: error!.localizedDescription)
                    })
                }
            }
        })

        task.resume()
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return mtours == nil ? 0 : (section == 0 ? 1 : filteredTours!.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return indexPath.section == 0 ? 60 : 135
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = indexPath.section == 0 ? "Header" : "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            let _cell = cell as! SegmentSwitchTableViewCell
            _cell.widthConstraint.constant = 320
            _cell.segmentSwitch.items = ["Все туры", "Пляжные", "Выходные", "Горные лыжи", "Экскурсии"]
            _cell.segmentSwitch.font = UIFont(name: "HelveticaNeue", size: 12.0)
            _cell.segmentSwitch.setSelectedIndex(selectedSegmentIndex, animated: false)
            _cell.segmentSwitch.addTarget(self, action: #selector(HotToursViewController.segmentSwitchDidChange(_:)), for: .valueChanged)
        }
        else
        {
            let _cell = cell as! HotTourTableViewCell
            let tour = filteredTours![indexPath.row]
            
            _cell.countryLabel.text = tour.country
            _cell.resortLabel.text = tour.resort
            _cell.periodLabel.text = "\(tour.period!) дней"
            _cell.priceLabel.text = "\(tour.price!) ₽"
            _cell.previewImageView.kf.setImage(with: URL(string: tour.preview)!)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HotTourRequestVC") as! RequestHotTourViewController
        viewController.tour = filteredTours![indexPath.row]
        
        let popupController = STPopupController(rootViewController: viewController)
        popupController.transitionStyle = STPopupTransitionStyle.fade
        popupController.containerView.layer.cornerRadius = 4
        popupController.present(in: self)
    }
    
    func segmentSwitchDidChange(_ sender: AnimatedSegmentSwitch)
    {
        if sender.selectedIndex != selectedSegmentIndex
        {
            selectedSegmentIndex = sender.selectedIndex
            filteredTours = mtours?.tours.filter(
            { (item: Tour) -> Bool in
                
                switch selectedSegmentIndex
                {
                    case 0: return true
                    case 1: return item.tourType == "Пляжный отдых" ? true : false
                    case 2: return item.tourType == "Weekends" ? true : false
                    case 3: return item.tourType == "Горные лыжи" ? true : false
                    case 4: return item.tourType == "City-tours" ? true : false
                    default: return false
                }
            })
            tableView.reloadSections(IndexSet(integer: 1), with: UITableViewRowAnimation.automatic)
        }
    }
}
