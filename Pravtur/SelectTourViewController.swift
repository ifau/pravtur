//
//  SelectTourViewController.swift
//  pravtur
//
//  Created by ifau on 15/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import STPopup

class SelectTourViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var tableView: UITableView!
    fileprivate var selectedSegmentIndex = 0
    let dataSource = selectTourSharedDataSource
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "SegmentSwitchTableViewCell", bundle: nil), forCellReuseIdentifier: "SegmentSwitch")
        tableView.register(UINib(nibName: "TextLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "TextLabel")
        tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextField")
        tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "Button")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        tableView.reloadSections(IndexSet(integer: 1), with: .none)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return dataSource.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataSource.numberOfRowsInSection(section, selectedSegment: selectedSegmentIndex)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return dataSource.heightForRowAtIndexPath(indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = dataSource.cellIdentifierForRowAtIndexPath(indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        configureCell(cell, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        if let viewController = dataSource.popupViewControllerForIndexPath(indexPath)
        {
            let popupController = STPopupController(rootViewController: viewController)
            popupController.style = STPopupStyle.bottomSheet
            popupController.present(in: self)
        }
    }
    
    func configureCell(_ cell: UITableViewCell, indexPath: IndexPath)
    {
        if cell is SegmentSwitchTableViewCell
        {
            let _cell = cell as! SegmentSwitchTableViewCell
            _cell.segmentSwitch.setSelectedIndex(selectedSegmentIndex, animated: false)
            _cell.segmentSwitch.items = dataSource.segmentSwitchItems()
            _cell.segmentSwitch.addTarget(self, action: #selector(SelectTourViewController.segmentSwitchDidChange(_:)), for: .valueChanged)
        }
        else if cell is TextLabelTableViewCell
        {
            let _cell = cell as! TextLabelTableViewCell
            _cell.firstLabel.text = dataSource.textLabelTitleForRow(indexPath.row)
            _cell.secondLabel.text = dataSource.textLabelValueForRow(indexPath.row)
            _cell.iconImageView.image = dataSource.textLabelIconForRow(indexPath.row)
        }
        else if cell is TextFieldTableViewCell
        {
            let _cell = cell as! TextFieldTableViewCell
            _cell.firstLabel.text = dataSource.textFieldTitleForRow(indexPath.row)
            _cell.textField.text = dataSource.textFieldValueForRow(indexPath.row)
            _cell.textField.tag = indexPath.row
            _cell.textField.keyboardType = indexPath.row == 0 ? UIKeyboardType.default : UIKeyboardType.phonePad
            _cell.textField.addTarget(self, action: #selector(SelectTourViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        }
        else if cell is ButtonTableViewCell
        {
            let _cell = cell as! ButtonTableViewCell
            _cell.title = dataSource.buttonTitleForSelectedSegment(selectedSegmentIndex)
            _cell.button.addTarget(self, action: #selector(SelectTourViewController.buttonDidPressed(_:)), for: UIControlEvents.touchUpInside)
        }
    }
    
    func segmentSwitchDidChange(_ sender: AnimatedSegmentSwitch)
    {
        if sender.selectedIndex != selectedSegmentIndex
        {
            selectedSegmentIndex = sender.selectedIndex
            let indexSet = NSMutableIndexSet()
            indexSet.add(1)
            indexSet.add(2)
            indexSet.add(3)
            tableView.reloadSections(indexSet as IndexSet, with: .automatic)
        }
    }
    
    func textFieldDidChange(_ textField: UITextField)
    {
        if textField.tag == 0
        {
            dataSource.name = textField.text
        }
        else if textField.tag == 1
        {
            dataSource.phone = textField.text
        }
    }
    
    func buttonDidPressed(_ sender: UIButton)
    {
        if (selectedSegmentIndex == 1) && dataSource.phone.characters.count < 5
        {
            return
        }
        
        let identifier = selectedSegmentIndex == 0 ? "RequestVC" : "PhoneVC"
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
        let popupController = STPopupController(rootViewController: viewController)
        popupController.transitionStyle = STPopupTransitionStyle.fade
        popupController.containerView.layer.cornerRadius = 4
        popupController.present(in: self)
    }
}


class SelectTourDataSource: NSObject
{
    func numberOfSections() -> Int
    {
        return 4
    }
    
    func cellIdentifierForRowAtIndexPath(_ indexPath: IndexPath) -> String
    {
        switch indexPath.section
        {
            case 0: return "SegmentSwitch"
            case 1: return "TextLabel"
            case 2: return "TextField"
            case 3: return "Button"
            default: return "Cell"
        }
    }
    
    func heightForRowAtIndexPath(_ indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section
        {
            case 0: return 58.0
            case 1: return 80.0
            case 2: return 80.0
            case 3: return 80.0
            default: return 44.0
        }
    }
    
    func numberOfRowsInSection(_ section: Int, selectedSegment: Int) -> Int
    {
        switch section
        {
            case 0: return 1
            case 1: return selectedSegment == 0 ? 3 : 0
            case 2: return selectedSegment == 0 ? 0 : 2
            case 3: return 1
            default: return 0
        }
    }
    
    func segmentSwitchItems() -> [String]
    {
        return ["Подобрать туры", "Обратный звонок"]
    }
    
    func textLabelTitleForRow(_ row: Int) -> String
    {
        let titles = ["Страна для отдыха", "Дата вылета", "Бюджет"]
        return titles[row]
    }
    
    var country = "Любая страна"
    var date = (Calendar.current as NSCalendar).date(byAdding: .month, value: 1, to: Date(), options: [])!
    var startNight = 7
    var endNight = 21
    var startPrice = 1
    var endPrice = 10
    
    func textLabelValueForRow(_ row: Int) -> String
    {
        switch row
        {
            case 0:
                return country
                
            case 1:
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM"
                return "\(dateFormatter.string(from: date)) на \(startNight) – \(endNight) ночей"
                
            case 2:
                return "От \(startPrice)0 000 до \(endPrice)0 \(endPrice == 500 ? "000 ₽ и более" : "000 ₽")"
                
            default:
                return ""
        }
    }
    
    func textLabelIconForRow(_ row: Int) -> UIImage?
    {
        let icons = ["IconLocation", "IconCalendar", "IconMoney"]
        return UIImage(named: icons[row])
    }
    
    func textFieldTitleForRow(_ row: Int) -> String
    {
        let titles = ["Ваше имя", "Ваш телефон*"]
        return titles[row]
    }
    
    var name: String!
    var phone: String!
    var bonus: String!
    
    override init()
    {
        name = (UserDefaults.standard.value(forKey: "UserName") as? String) ?? ""
        phone = (UserDefaults.standard.value(forKey: "UserPhone") as? String) ?? ""
        bonus = (UserDefaults.standard.value(forKey: "UserBonus") as? String) ?? ""
    }
    
    func textFieldValueForRow(_ row: Int) -> String
    {
        switch row
        {
            case 0:
                return name
            case 1:
                return phone
            default:
                return ""
        }
    }
    
    func buttonTitleForSelectedSegment(_ segment: Int) -> String
    {
        let titles = ["Подобрать", "Позвоните мне!"]
        return titles[segment]
    }
    
    func popupViewControllerForIndexPath(_ indexPath: IndexPath) -> UIViewController?
    {
        if indexPath.section == 1
        {
            switch indexPath.row
            {
                case 0: return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectCountryVC")
                case 1: return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectDateVC")
                case 2: return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectPriceVC")
                default: return nil
            }
        }
        return nil
    }
    
    func sendTourRequestToRemoteServer(_ completion: @escaping (_ success: Bool) -> ())
    {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
//            completion(success: false)
//        })
        let session = URLSession.shared
        let str = "http://tuiapi.azurewebsites.net/api/TourSelection"
        let url = URL(string: str)!
        var request = URLRequest(url: url)
        
        var dict = Dictionary<String,String>()
        dict["Name"] = name
        dict["Phone"] = phone
        dict["Country"] = country
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        dict["StartDate"] = dateFormatter.string(from: date)
        dict["NightsCount"] = "\(startNight) - \(endNight)"
        dict["Cost"] = "\(startPrice)0 000 - \(endPrice)0 000"
        dict["Description"] = ""
        dict["CardNumber"] = bonus
        
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [])
        
        request.httpBody = data
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil && data != nil
            {
                DispatchQueue.main.async(execute: { completion(true) })
            }
            else
            {
                DispatchQueue.main.async(execute: { completion(false) })
            }
        }) 
        
        task.resume()
    }
    
    func sendPhoneRequestToRemoteServer(_ completion: @escaping (_ success: Bool) -> ())
    {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(3 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
//            completion(success: true)
//        })
        let session = URLSession.shared
        let str = "http://tuiapi.azurewebsites.net/api/Callback"
        let url = URL(string: str)!
        var request = URLRequest(url: url)
        
        var dict = Dictionary<String,String>()
        dict["Name"] = name
        dict["Phone"] = phone
        
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [])
        
        request.httpBody = data
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil && data != nil
            {
                DispatchQueue.main.async(execute: { completion(true) })
            }
            else
            {
                DispatchQueue.main.async(execute: { completion(false) })
            }
        }) 
        
        task.resume()
    }
    
    func sendHotTourRequestToRemoteServer(_ tour: Tour, completion: @escaping (_ success: Bool) -> ())
    {
        let session = URLSession.shared
        let str = "http://aviumemailsender.azurewebsites.net/api/Sender"
        let url = URL(string: str)!
        var request = URLRequest(url: url)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: tour.departure)!
        dateFormatter.dateFormat = "dd MMMM yyyy"
        
        var body = ""
        body += "Заявка на резервирование горящего тура.\n"
        body += "Источник тура: mtours.ru\n"
        body += "\n"
        body += "Имя клиента: \(name!)\n"
        body += "Номер телефона: \(phone!)\n"
        body += "Номер бонусной карты: \(bonus!)\n"
        body += "\n"
        body += "Информация о туре:\n"
        body += "ID: \(tour.id!)\n"
        body += "Оператор: \(tour.operator_!)\n"
        body += "Страна: \(tour.country!)\n"
        body += "Курорт: \(tour.resort!)\n"
        body += "Отель: \(tour.hotel!)\n"
        body += "Количество дней: \(tour.period!)\n"
        body += "Дата вылета: \(dateFormatter.string(from: date))\n"
        body += "Стоимость: \(tour.price!)\n"
        body += "В тур включено: \(tour.contents!)\n"
        body += "Дополнительно: Цена указана на 1 человека при 2-местном размещении."
        
        var dict = Dictionary<String,String>()
        dict["To"] = "tuischuk@yandex.ru"
        dict["Subject"] = "Резервирование горящего тура"
        dict["Body"] = body
        
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [])
        
        request.httpBody = data
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil && data != nil
            {
                DispatchQueue.main.async(execute: { completion(true) })
            }
            else
            {
                DispatchQueue.main.async(execute: { completion(false) })
            }
        }) 
        
        task.resume()
    }
}

var selectTourSharedDataSource = SelectTourDataSource()
