//
//  ContactsViewController.swift
//  pravtur
//
//  Created by ifau on 06/04/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController
{
    @IBOutlet var textView: UITextView!
    
    @IBOutlet var vkButton: UIButton!
    @IBOutlet var instagramButton: UIButton!
    @IBOutlet var facebookButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let descriptionText = "Звоните: +7 (495) 374-84-01\n\nПишите: WhatsApp, Viber +7 (925) 805-22-62\n\nПриходите: Москва, метро Щукинская, Маршала Василевского ул. 17"
        let attributedDescription = NSMutableAttributedString(string: descriptionText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 14)!, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSLinkAttributeName, value: URL(string: "http://maps.apple.com/?ll=55.912188,37.418533&z=16")!, range: (descriptionText as NSString).range(of: "Москва, метро Щукинская, Маршала Василевского ул. 17"))
        
        textView.attributedText = attributedDescription
        
        vkButton.layer.cornerRadius = 22
        vkButton.layer.borderWidth = 1
        vkButton.layer.borderColor = UIColor.white.cgColor
        
        instagramButton.layer.cornerRadius = 22
        instagramButton.layer.borderWidth = 1
        instagramButton.layer.borderColor = UIColor.white.cgColor
        
        facebookButton.layer.cornerRadius = 22
        facebookButton.layer.borderWidth = 1
        facebookButton.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func vkButtonPressed(_ sender: AnyObject)
    {
        UIApplication.shared.openURL(URL(string: "http://vk.com/pravilnoe_tur")!)
    }
    
    @IBAction func instagramButtonPressed(_ sender: AnyObject)
    {
        UIApplication.shared.openURL(URL(string: "https://www.instagram.com/pravdatur")!)
    }
    
    @IBAction func facebookButtonPressed(_ sender: AnyObject)
    {
        UIApplication.shared.openURL(URL(string: "https://www.facebook.com/traveliser")!)
    }
}
