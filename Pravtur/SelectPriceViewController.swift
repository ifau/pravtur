//
//  SelectPriceViewController.swift
//  pravtur
//
//  Created by ifau on 24/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class SelectPriceViewController: UIViewController, YSRangeSLiderDelegate
{
    @IBOutlet var sliderView: YSRangeSlider!
    @IBOutlet var priceLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sliderView.minimumValue = 1
        sliderView.maximumValue = 50
        sliderView.minimumSelectedValue = CGFloat(selectTourSharedDataSource.startPrice)
        sliderView.maximumSelectedValue = CGFloat(selectTourSharedDataSource.endPrice)
        sliderView.leftThumbColor = Constants.blueColor()
        sliderView.rightThumbColor = Constants.blueColor()
        sliderView.sliderLineColorBetweenThumbs = Constants.blueColor()
        sliderView.sliderLineColor = UIColor.lightGray
        sliderView.delegate = self
        
        priceLabel.textColor = Constants.blueColor()
        updateLabel()
    }
    
    func rangeSliderDidChange(_ rangeSlider: YSRangeSlider, minimumSelectedValue: CGFloat, maximumSelectedValue: CGFloat)
    {
        selectTourSharedDataSource.startPrice = Int(minimumSelectedValue)
        selectTourSharedDataSource.endPrice = Int(maximumSelectedValue)
        updateLabel()
    }
    
    func updateLabel()
    {
        let startPrice = selectTourSharedDataSource.startPrice
        let endPrice = selectTourSharedDataSource.endPrice
        priceLabel.text = "От \(startPrice)0 000 до \(endPrice)0 \(endPrice == 50 ? "000 ₽ и более" : "000 ₽")"
    }
}
